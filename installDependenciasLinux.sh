#!/bin/bash

echo "Instalando Dependencias Linux - Null Client..."
echo "  >>  libopenal1"
echo "  >>  libglew2.0"
echo
echo "Utilizando aptitude para isso..."
echo "Rodando comando: apt-update -qq; apt install -y -qq libglew2.1 libopenal1"
echo "Instalando..."
apt update -qq; apt install -y -qq libglew2.1 libopenal1
echo "Instalado"
echo "Criando link simbolico..."
sudo ln -f -s /usr/lib/x86_64-linux-gnu/libGLEW.so.2.1 /usr/lib/libGLEW.so.2.0
echo "Link Criado"
echo "Abrindo cliente..."
./client-linux &
echo "Finalizado..."